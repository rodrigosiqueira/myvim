" Enable error files & error jumping.
setlocal cf
" Number of things to remember in history.
setlocal history=512
" Writes on make/shell commands
setlocal autowrite
" Ruler on
setlocal ruler
" Line wrapping off
setlocal nowrap
" Time to wait after ESC (default causes an annoying delay)
setlocal timeoutlen=250
" colorscheme vividchalk  " Uncomment this to set a default theme
" Formatting
setlocal nocp incsearch
setlocal cinoptions=:0,p0,t0
setlocal cinwords=if,else,while,do,for,switch,case
setlocal formatoptions=tcqr
setlocal cindent
setlocal autoindent
setlocal smarttab
setlocal expandtab

" Visual
" Show matching brackets.
setlocal showmatch
" Bracket blinking.
setlocal mat=5
setlocal list
" Show $ at end of line and trailing space as
setlocal lcs=tab:\ \ ,eol:$,trail:~,extends:>,precedes:<
" No blinking.
setlocal novisualbell
" No noise.
setlocal noerrorbells
" Always show status line.
setlocal laststatus=2 
