setlocal autoindent
setlocal tabstop=8
setlocal softtabstop=8
setlocal shiftwidth=8
setlocal noexpandtab

setlocal list
setlocal lcs=tab:\ \ ,eol:$,trail:~,extends:>,precedes:<
